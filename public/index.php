<?php
require "../data/people.php";
require "../data/meetings.php";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Appuntamenti</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">
					Appuntamenti
				</a>
			</div>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Appuntamenti</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped">
					<thead>
						<th>Data</th>
						<th>Ora</th>
						<th>Nome</th>
						<th>Luogo</th>
						<th>Partecipanti</th>
					</thead>
					<tbody>
						<?php
						foreach($appuntamenti as $key => $appuntamento) {
							?>
							<tr>
								<td><?php echo $appuntamento["data"]; ?></td>
								<td><?php echo $appuntamento["ora"]; ?></td>
								<td><?php echo $appuntamento["nome"]; ?></td>
								<td><?php echo $appuntamento["luogo"]; ?></td>
								<td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal<?php echo $key; ?>">Mostra</button></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<?php foreach ($appuntamenti as $key => $appuntamento ) { ?>
	<div class="modal fade" id="modal<?php echo $key; ?>" tabindex="-1" role="dialog" aria-labelledby="modal<?php echo $key; ?>Label">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="modal<?php echo $key; ?>Label">Partecipanti</h4>
				</div>
				<div class="modal-body">
					<ul>
						<?php foreach ($appuntamento["partecipanti"] as $partecipante) { ?>
						<li><?php echo $persone[$partecipante]["nome"]." ".$persone[$partecipante]["cognome"]; ?></li>
						<?php } ?>
					</ul>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>
